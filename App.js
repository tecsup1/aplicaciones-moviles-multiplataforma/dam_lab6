import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Alert,
  Button,
} from 'react-native';

import OurFlatList from './components/ourFlatList/OurFlatList';
import ConexionFetch from './components/conexionFetch/ConexionFetch';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
//const Stack = createStackNavigator();

//lab6:
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
//const Tab = createBottomTabNavigator();

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
//const Tab = createMaterialBottomTabNavigator();

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
const Tab = createMaterialTopTabNavigator();


function HomeScreen({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>
      <Button
        title="Go to details - Gonzalo´s Project"
        onPress={() => navigation.navigate('Details')}
      />
    </View>
  );
}

function DetailsScreen({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Details screen</Text>

      <Button
        title="Go back to first screenin stack - Gonzalo´s Project"
        onPress={() => navigation.popToTop()}
      />
    </View>
  );
}

class App extends Component {
  onItemClick(e) {
    console.warn('You selected the movie: ' + e);
  }

  showAlert = () => {
    Alert.alert(
      'Titulo',
      'Mensaje',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => console.log('OK pressed')},
      ],
      {cancelable: false},
    );
  };

  render() {
    return (
      //<View style={styles.container}>
      // <ConexionFetch onItemClick={this.onItemClick} />
      // <OurFlatList showAlert={this.showAlert} />
      // </View>

      <NavigationContainer>
        {/*<Stack.Navigator>
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Details" component={DetailsScreen} />
        </Stack.Navigator>*/}

        {/*<Tab.Navigator>
          <Tab.Screen name="Home" component={HomeScreen} />
          <Tab.Screen name="Details" component={DetailsScreen} />
        </Tab.Navigator>*/}

        <Tab.Navigator
          initialRouteName="Home"
          tabBarOptions={{
            activeTintColor: '#e91e63',
          }}>
          <Tab.Screen
            name="Home"
            component={HomeScreen}
            options={{
              tabBarLabel: 'Home',
              tabBarIcon: ({color, size}) => (
                <MaterialCommunityIcons name="home" color={color} size={size} />
              ),
            }}
          />

          <Tab.Screen
            name="Details"
            component={DetailsScreen}
            options={{
              tabBarLabel: 'Details',
              tabBarIcon: ({color, size}) => (
                <MaterialCommunityIcons name="bell" color={color} size={size} />
              ),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
